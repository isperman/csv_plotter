import argparse
import matplotlib.pyplot as plt
import sys
from matplotlib.widgets import Cursor
import os
import signal

parser = argparse.ArgumentParser(description='Plot data from CSV files')

parser.add_argument('-f',
                    dest='filename',
                    required=True,
                    help='file to manipulate')

parser.add_argument('--header',
                    dest='header',
                    action='store_true',
                    help='set this if you have 1 header line')

parser.add_argument('-headers',
                    dest='headers',
                    help='specify how many header rows your file has')

parser.add_argument('--tek',
                    dest='tektronix',
                    action='store_true',
                    help='set this if it\'s an export from an tektronix oscilloscope')

parser.add_argument('--zi',
                    dest='zhinst',
                    action='store_true',
                    help='set this if it\'s an export form the Zurich Instruments\' ziControl')

parser.add_argument('--logic',
                    dest='logic',
                    action='store_true',
                    help='set this if it\'s an export from a saleae logic analyzer')

parser.add_argument('--plotall',
                    dest='plotall',
                    action='store_true',
                    help='set this if in the first column you have the time vector and in the next ones the data')

args = parser.parse_args()

print('filename: {}'.format(args.filename))
print('with header? {}'.format(args.header))
print('Tektronix format? {}'.format(args.tektronix))
print('ziControl format? {}'.format(args.zhinst))
print('Logic format? {}'.format(args.logic))
print('Plot all? {}'.format(args.plotall))
print('Number of headers: {}'.format(args.headers))

#os.kill(os.getppid(), signal.SIGHUP)

data = list()

if args.header:
    if args.headers == None:
        nr_headers = 1
    else:
        nr_headers = int(args.headers)
else:
    if args.headers != None:
        nr_headers = int(args.headers)
    else:
        nr_headers = 0


if args.tektronix:
    with open(args.filename) as inFile:
        data = inFile.readlines()
elif args.zhinst:
    with open(args.filename) as inFile:
        data = inFile.readlines()
elif args.logic:
    with open(args.filename) as inFile:
        data = inFile.readlines()[nr_headers:]
elif args.plotall:
    with open(args.filename) as inFile:
        data = inFile.readlines()[nr_headers:]

        row0 = data[0]
        elements = row0.split(',')

        ordinates = dict()
        for el in range(0, len(elements)-1):
            ordinates[el] = list()
else:
    print("you need to specify the data format!")
    sys.exit()

abscissa = list()
ordinate_1 = list()
ordinate_2 = list()


for line in data:
    row_elements = line.split(',')
    if args.tektronix:
        abscissa.append(row_elements[3])
        ordinate_1.append(row_elements[4])
    if args.logic:
        abscissa.append(row_elements[0])
        ordinate_1.append(row_elements[2])
    if args.zhinst:
        abscissa.append(row_elements[1])
        ordinate_1.append(row_elements[7]) # Amplitude
        ordinate_2.append(row_elements[8]) # Phase
    if args.plotall:
        abscissa.append(row_elements[0])
        for nr in range(0, len(row_elements)-1):
            ordinates[nr].append(row_elements[nr+1])

if args.zhinst:
    plt.figure(1)

    ax1 = plt.subplot(211)
    plt.xscale('log')
    #plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude (dB)')
    plt.grid(True)
    plt.xticks([])
    frame1 = plt.gca()
    frame1.axes.get_xaxis().set_ticks([])
    plt.plot(abscissa, ordinate_1)

    ax2 = plt.subplot(212, sharex=ax1)
    plt.xscale('log')
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Phase (deg')
    plt.grid(True)
    plt.plot(abscissa, ordinate_2)

    cursor1 = Cursor(ax1, useblit=True, color='green', linewidth=1)
    cursor2 = Cursor(ax2, useblit=True, color='green', linewidth=1)

    plt.show()

if args.plotall:
    plt.figure(1)

    axes = dict()
    cursors = dict()

    for el in range(0, len(ordinates)):

        if len(axes) == 0:
            axes[el+1] = plt.subplot(len(ordinates), 1, el+1)
        else:
            axes[el+1] = plt.subplot(len(ordinates), 1, el+1, sharex=axes[1])
        plt.xlabel('Time')
        plt.ylabel('Voltage')
        plt.grid(True)

        min_value = float(min(ordinates[el]))
        max_value = float(max(ordinates[el]))

        d_range = max_value - min_value

        if(d_range == 0):
            min_axis = min_value - 0.1
            max_axis = max_value + 0.1
        else:
            min_axis = min_value - 0.15*d_range
            max_axis = max_value + 0.15*d_range
        #axes[el+1].set_ylim([min_axis, max_axis])

        frame1 = plt.gca()
        plt.plot(abscissa, ordinates[el])
        cursors[el+1] = Cursor(axes[el+1], useblit=True, color='red', linewidth=1)


    plt.show()


else:
    plt.plot(abscissa, ordinate_1)
    plt.grid(True)
    plt.ylabel('Voltage')
    plt.xlabel('Time')
    plt.show()
